---
title: "A few notes on Gitlab"
date: 2018-03-06T13:56:59Z
draft: false
---

## GitLab and GitKraken

Gitlab hosts a Git code repositories, it is a container registry and it also has Continuous Integration (CI) to automate builds.  Like Github, Gitlab hosts static sites written in Markdown.   Its CI service supports static site generators like Hugo.  Pushing Hugo markdown files to the Gitlab repository can trigger the Hugo build process.  This page was generated in this way.

The workflow was:

Use the terminal and hugo commands to create a markdown file for a page with the required header (frontmatter).  Edit the frontmatter to provide a meaningful title for the page and set draft to false.  Then add content such as this...

Save the file locally in a git managed dir that has Gitlab set as a remote repository.  If the Git CLI were to be used, it would be a case of adding the file to local index, then commiting it and finally pushing the changes to the Gitlab remote repo.  However, in this case the GitKraken GUI has been used.  This required the local and remote repos to be configured.  There is a tab for Gitlab where the public and private keys can be set so GitKraken can push to Gitlab.  There are similar tabs for Bitbucket and Github.

Gitkraken was used to add, commit and push the changes to Gitlab and the additional file could be see in the Gitlab admin page for the hosted repo.   The pipeline page showed that a job was pending and, after a few minutes this changed to complete and then a change to the web page could be seen in a browser.

This is the url 

https://filmstar.gitlab.io/sitec/pages/page7/

So, let me take this from the top with this latest edit:

Save the file in Typora

In GitKraken it says one file changed in commit panel and there is a box to edit the last commit message.   "Changed page7 title to a few notes on Gilab and added Gitlab and Karken article" ......oops that was to change an existing commit message.   What I need to do click on the page7.md file and stage the file.   Under that there is room for my new a commit message.

I need to find a way to take screen shots of this.

OK, so the change is committed, now I have to push.  There is a push tab on the top menu of GitKraken.   Now check that it has reached Gitlab.

We have a pending job that has not been picked up by a Gitlab runner.   Runners are build containers.   In this case it is a container with Hugo installed.  It will copy all the markdown files in the Gitlab repo and use them to generate a website that it hosts on Gitlab.  Here is the hidden .gitlab-ci.yml file that controls this process:

```
cat .gitlab-ci.yml
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master

```

It uses a hugo image to create the container and runs hugo placing its output in dir /public.

Need to understand the Git submodule stuff.

Here is the log of taken from Gitlab after the runner has done its job.

The job may be pending for a few minutes until a shared runner becomes availbale.

```bash
Running with gitlab-runner 10.5.0 (80b03db9)
  on docker-auto-scale 72989761
Using Docker executor with image monachus/hugo ...
Pulling docker image monachus/hugo ...
Using docker image sha256:a26913f0de77db01d50e77183da24bbdbf0c6e19d2bef8224d86533e82c6d442 for monachus/hugo ...
Running on runner-72989761-project-5485534-concurrent-0 via runner-72989761-srm-1520433488-ae8d4552...
Cloning repository...
Cloning into '/builds/Filmstar/sitec'...
Checking out 0a03a5d4 as master...
Updating/initializing submodules recursively...
Submodule 'themes/ananke' (https://github.com/budparr/gohugo-theme-ananke.git) registered for path 'themes/ananke'
Cloning into '/builds/Filmstar/sitec/themes/ananke'...
Submodule path 'themes/ananke': checked out 'd5b87cb15e820e3ee8335363b26f4fcbf0e8d783'
$ hugo
Building sites … 

                   | EN  
+------------------+----+
  Pages            | 21  
  Paginator pages  |  0  
  Non-page files   |  0  
  Static files     |  3  
  Processed images |  0  
  Aliases          |  2  
  Sitemaps         |  1  
  Cleaned          |  0  

Total in 163 

Uploading artifacts...
public: found 48 matching files                    
Uploading artifacts to coordinator... ok            id=56227826 responseStatus=201 Created token=hFJzUSBk
Job succeeded
```

We can now see the update web page.

GitKraken makes things a lot easier because it avoids the common mistakes using the git and hugo CLI.

It would be  nice to have a notification when the page is built.













